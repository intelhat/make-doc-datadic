<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'ProgressController@connect_info');

// Route::get('form-connect', 'ProgressController@form_connect');
Route::get('connect-info', 'ProgressController@connect_info');
Route::get('document-datadic', 'ProgressController@document_datadic');
Route::get('preview-datadic', 'ProgressController@preview_datadic');
Route::get('update-table-info', 'ProgressController@update_table_info');
