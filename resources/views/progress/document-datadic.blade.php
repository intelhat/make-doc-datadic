@extends('layouts.themes1.content')

@push('css')
<style>
    #form-connect label {
        font-weight:bold;
    }
</style>
@endpush

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-12">

            @php
            // กำหนด comment ของตาราง
            $tableCommentSet = [
				'advertises'           => 'ไฮไลท์กิจกรรม',
				'calendar_types'       => 'ประเภทปฏิทินกิจกรรม',
				'calendars'            => 'ปฏิทินกิจกรรม',
				'cirletter_types'      => 'ประเภทหนังสือเวียน',
				'cirletters'           => 'หนังสือเวียน',
				'cnf_department'       => 'กรม',
				'cnf_division'         => 'กอง/ศำนัก',
				'cnf_prename'          => 'คำนำหน้าชื่อ',
				'cnf_workgroup'        => 'กลุ่มงาน',
				'document_files'       => 'ไฟล์แนบ',
				'logs'                 => 'บันทึกการใช้งาน',
				'news'                 => 'ข่าวสาร',
				'permission'           => 'สิทธิ์การใช้งาน',
				'permission_detail'    => 'รายละเอียดสิทธิ์การใช้งาน',
				'permission_group'     => 'กลุ่มสิทธิ์การใช้งาน',
				'publish_documents'    => 'การเผยแพร่เอกสาร',
				'publish_types'        => 'ประเภทการเผยแพร่',
				'sharefile_attributes' => 'ข้อจำกัดไฟล์แนบ',
				'sharefile_types'      => 'ประเภทแชร์ไฟล์',
				'sharefiles'           => 'แชร์ไฟล์',
				'users'                => 'ผู้ใช้งาน',
				'videos'               => 'วิดีโอ',
				'weblinks'             => 'เว็บลิงค์',
            ];
            
            // กำหนด default comment ของ field
            $colCommentSet = [
                'id' => 'รหัสข้อมูล',
                'code' => 'รหัส',
                'name' => 'ชื่อ',
                'status_id' => 'รหัสสถานะ',
                'created_at' => 'วันที่สร้างรายการ',
                'created_by_id' => 'รหัสผู้สร้างรายการ',
                'created_by' => 'ชื่อผู้สร้างรายการ',
                'updated_at' => 'วันที่ปรับปรุงรายการ',
                'updated_by_id' => 'รหัสผู้ปรับปรุงรายการ',
                'updated_by' => 'ชื่อผู้ปรับปรุงรายการ',
        
                'start_date' => 'วันที่เริ่มต้น',
                'end_date' => 'วันที่สิ้นสุด',
                'time' => 'ครั้งที่',
                'year' => 'ปี',
            ];
            
                
                $tableList = [];
                $tables = \DB::select('SHOW TABLES');
                foreach($tables as $table) 
				{
                    $tableName    = $table->{'Tables_in_'.env('DB_DATABASE')};
                    $tableComment = @$tableCommentSet[$tableName]?:false;
                    // if($tableComment)
                        $tableList[] = ['name' => $tableName, 'comment' => $tableComment];
                }
            
                echo '<table class="table table-bordered">'
                        .'<thead>'
                            .'<tr>'
                                .'<th>ลำดับ</th>'
                                .'<th>Table Name</th>'
                                .'<th>Description</th>'
                                .'<th>Remark</th>'
                            .'</tr>'
                        .'</thead>'
                        .'<tbody>';
            
                    foreach($tableList as $item) {
                        echo '<tr>'
                            .'<td>'.(empty($no)?$no = 1:++$no).'</td>'
                            .'<td>'.$item['name'].'</td>'
                            .'<td>'.($item['comment']?:'ตารางข้อมูล').'</td>'
                            .'<td></td>'
                        .'</tr>';
                    }
            
                echo '</tbody>'
                    .'</table>';
                echo '<br><br><br>';
            
                unset($no);
                foreach($tableList as $table) {
                    $colInfo = DB::select('SHOW FULL COLUMNS FROM '.$table['name']);
            
                    echo '<div><strong>ตารางที่ '.(empty($no)?$no = 1:++$no).': </strong>'.$table['name'].'</div>';
                    echo '<div><strong>รายละเอียด: </strong> '.$table['comment'].'</div>';
                    echo '<table class="table table-bordered">'
                        .'<thead>'
                            .'<tr>'
                                .'<th>Field Name</th>'
                                .'<th>Field Type</th>'
                                .'<th>Allow Null</th>'
                                .'<th>KEY</th>'
                                .'<th>Description</th>'
                            .'</tr>'
                        .'</thead>'
                        .'<tbody>';
            
            
                        foreach($colInfo as $col) {
                            $comment = $col->Comment?:@$colCommentSet[$col->Field]?:'';
                            echo '<tr>'
                                .'<td>'.$col->Field.'</td>'
                                .'<td>'.$col->Type.'</td>'
                                .'<td>'.ucfirst($col->Null).'</td>'
                                .'<td>'.($col->Key=='PRI'?'PK':'').'</td>'
                                .'<td>'.$comment.'</td>'
                            .'</tr>';
                        }
            
                    echo '</tbody>'
                    .'</table>';
            
                    echo '<br><BR>';
                }
        @endphp


        </div>
    </div>
</div>


@endsection