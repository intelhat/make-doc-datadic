@extends('layouts.themes1.content')

@push('css')
<style>
    label {
        font-weight:bold;
    }
    #form-connect label {
        font-weight:bold;
    }
</style>
@endpush

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">


            <div style="margin-top:40%;">
                <div class="mb-3">
                    <h3>Connect info:</h3>
                </div>

                <div>
                    <h5>Connect Status:</h5>
                    <hr>
                </div>

                <div class="mb-3">
                    <label for="">Connect status:</label>
                    <div> {!! $connect_status_message !!} </div>
                </div>

                <br>
                <div>
                    <h5>Connect info:</h5>
                    <hr>
                </div>
                <div class="mb-3">
                    <label for="">Host:</label>
                    <div> {{ env('DB_HOST') }} </div>
                </div>

                <div class="mb-3">
                    <label for="">Port:</label>
                    <div> {{ env('DB_PORT') }} </div>
                </div>
    
                <div class="mb-3">
                    <label for="">Username:</label>
                    <div> {{ env('DB_USERNAME') }} </div>
                </div>
    
                <div class="mb-3">
                    <label for="">Password:</label>
                    <div>******</div>
                </div>

                <div class="mb-3">
                    <label for="">Database:</label>
                    <div> {{ env('DB_DATABASE') }} </div>
                </div>

                <div>
                    @if($connect_status)
                        <div class="mb-2"><a href="{{ url('preview-datadic') }}" class="btn  btn-info w-100" target="_blank">Preview Data dictionary</a></div>
                        <a href="{{ url('document-datadic') }}" class="btn  btn-success w-100">Create Data dictionary</a>
                    @else
                        <button type="submit" class="btn  btn-danger w-100 disabled">Fail Connect</button>
                    @endif
                </div>
            </div>

            
        </div>
    </div>
</div>


@endsection