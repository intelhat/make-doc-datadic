@extends('layouts.themes1.content')

@push('css')
<style>
    #form-connect label {
        font-weight:bold;
    }
</style>
@endpush

@section('content')

{{ Form::open(['method' => 'post', 'url' => 'connect-info']) }}

<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">


            <div style="margin-top:40%;">
                <div class="mb-3">
                    <h3>Form Connect:</h3>
                </div>

                <div class="mb-3">
                    {{ Form::text('host', false, ['class'=>'form-control', 'placeholder' => 'Host', 'autofocus' => true]) }}
                </div>

                <div class="mb-3">
                    {{ Form::text('port', 3306, ['class'=>'form-control', 'placeholder' => 'Port']) }}
                </div>
    
                <div class="mb-3">
                    {{ Form::text('username', false, ['class'=>'form-control', 'placeholder' => 'Username']) }}
                </div>
    
                <div class="mb-3">
                    {{ Form::password('password', ['class'=>'form-control', 'placeholder' => 'Password']) }}
                </div>

                <div class="mb-3">
                    {{ Form::text('database', false, ['class'=>'form-control', 'placeholder' => 'Database']) }}
                </div>

                <div>
                    <button type="submit" class="btn  btn-primary w-100">Connect</button>
                </div>
            </div>

            {{-- <div class="card  shadow mt-5" id="form-connect">
                <div class="card-header">
                    <h5>Form connect:</h5>
                </div>
                <div class="card-body">


                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Host: </label>
                            <div>
                                {{ Form::text('host', false, ['class'=>'form-control', 'placeholder' => 'Host']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Username: </label>
                            <div>
                                {{ Form::text('username', false, ['class'=>'form-control', 'placeholder' => 'Username']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Password: </label>
                            <div>
                                {{ Form::password('password', ['class'=>'form-control', 'placeholder' => 'Password']) }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-end">
                    <button type="submit" class="btn btn-sm btn-primary">Connect</button>
                </div>
            </div> --}}
            
        </div>
    </div>
</div>

{{ Form::close() }}

@endsection