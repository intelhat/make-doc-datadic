{{-- รายชื่อข้อมูล ตาราง --}}
<table border="1">'
	<thead>
		<tr>
			<th>ลำดับ</th>
			<th>Table Name</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tables as $key => $table)
			@php
				$no = $key + 1;
			@endphp
			<tr>
				<td>{{ $no }}</td>
				<td>{{ $table['name'] }}</td>
				<td>{{ $table['comment'] }}</td>
			</tr>
		@endforeach
	</tbody>
</table> 
<br><Br>
