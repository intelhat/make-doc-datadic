{{-- รายละเอียด ข้อมูล ตาราง --}}
@foreach ($info as $item)
	<div>{{ 'ตารางที่'. $item['no'] .': '.$item['name'] }}</div>
	<div>{{ 'รายละเอียด: ตาราง'.$item['comment'] }}</div>

	<table border="1">
		<thead>
			<tr>
				<th>Field Name</th>
				<th>Field Type</th>
				<th>Allow Null</th>
				<th>KEY</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($item['info'] as $info_item)
				<tr>
					<td>{{ $info_item[0] }}</td>
					<td>{{ $info_item[1] }}</td>
					<td>{{ $info_item[2] }}</td>
					<td>{{ $info_item[3] }}</td>
					<td>{!! ($info_item[4]?:'<span style="color:#f00;">ไม่มีข้อมูล</span>') !!} </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<br>
@endforeach