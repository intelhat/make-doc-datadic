@include('vendor.bootstrap')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Make Documents Data dictionary:</title>

    @stack('css')
</head>
<body>

    @yield('blank-content')
    
    @stack('js')
</body>
</html>