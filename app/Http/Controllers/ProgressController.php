<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProgressController extends Controller
{
	private $is_oracle;

	public function __construct() 
	{
		$this->is_oracle = strtolower(env('DB_CONNECTION')) == 'oracle';;
	}

    //fill database connect
    public function form_connect() {
        return view('progress.form-connect');
    }

    // process connect to db
    private function connect_process() {
        // return status connect;
    }

    // show display database info
    public function connect_info(Request $req) 
	{
        try {
            \DB::connection()->getPdo();
            $connect_status = true;
        } catch (\Exception $e) {
            $connect_status = false;
        }

        if($connect_status) {
            $icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
            </svg>';
            $connect_status_message = '<span class="text-success">'.$icon.' Success connection.</span>';
        } else {
            $icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                <path d="M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"/>
            </svg>';
            $connect_status_message = '<span class="text-danger">'.$icon.' Fail connection.</span>';
        }

        return view('progress.connect-info', compact('connect_status', 'connect_status_message'));
    }

    // print document datadictionary
    public function document_datadic() 
	{
		set_time_limit(30);

        return $this->create_word();
        // return view('progress.document-datadic');
    }
// ==================================================
// GetData : 
// ==================================================
	/**
	 * Function: get_data()
	 * Description: คืนข้อมูลสำหรับใช้ทำรายงานทั้งหมด
	 */
    public function get_data() 
	{
		$tables = $this->getTableInfo();
		$info   = $this->getInfoByTables($tables);
		return compact('tables', 'info');
    }
	/**
	 * Function: getTableInfo()
	 * Description: 
	 * คืนข้อมูลเกี่ยวกับตาราง name (ชื่อตาราง), comment (คำอธิบาย)
	 * - comment_default - กำหนด comment default สำหรับกรณีที่ไม่มีการระบุ comment มาในฐานข้อมูล
	 * - exception_tables - ถ้ากำหนดชื่อตารางที่ต้องการยกเว้น จะไม่แสดงในรายงาน
	 */
	private function getTableInfo()
	{
		$database_name    = env('DB_DATABASE');
		$table_list       = [];

		$exception_tables = [
			'_blueprint',
			'ADF_FAMILIES',
			'ADF_FAMILY_ADOPT_DISABLE_TYPES',
			'FAILED_JOBS',
			'MIGRATIONS',
			'MODEL_HAS_PERMISSIONS',
			'MODEL_HAS_ROLES',
			'PASSWORD_RESET_TOKENS',
			'PERMISSIONS',
			'PERMS',
			'PERM_SYSTEMS',
			'PERSONAL_ACCESS_TOKENS',
			'ROLE_HAS_PERMISSIONS',
			'_20240311_ST_SUB_DISTRICTS',
		];


		$comment_default = 'ไม่ระบุ';
		$is_oracle       = $this->is_oracle;

		if ($is_oracle)
			$qry = "
				SELECT 
					table_name,
					comments AS table_comment
				FROM 
					all_tab_comments
				WHERE 
					owner = '".strtoupper(env('DB_DATABASE'))."'
			";
		else
			$qry = "
				SELECT 
					table_name,
					tablde_comment
				FROM 
					information_schema.tables
				WHERE 
					table_schema = '".strtolower(env('DB_DATABASE'))."';
			";
		// Data raw: table list
		$tables = \DB::select($qry);
		// Result:
		$results = [];
		foreach ($tables as $table)
		{
			$name    = $table->table_name;
			$comment = $table->table_comment ? : $comment_default;

			$results[] = ['name' => $name, 'comment' => $comment];
		}
		$table_list = $results;

		// Exception Table remove:
		if ($exception_tables)
		{
			foreach ($table_list as $key => $item)
			{
				$is_exception = in_array($item['name'], $exception_tables);
				if ($is_exception)
					unset($table_list[$key]);
			}
			sort($table_list);
		}
		return $table_list;
	}
	/**
	 * Function: getInfoByTables()
	 * Description: ใช้ข้อมูล table มาทำข้อมูล info
	 * - ข้อมูล info เป็นข้อมูล field ตามที่ถูกออกแบบในระบบ
	 */
	private function getInfoByTables($tables = [])
	{
		$info = [];
		$is_oracle = $this->is_oracle;
		$database_name = $is_oracle 
			? strtoupper(env('DB_DATABASE')) 
			: strtolower(env('DB_DATABASE')) ;

		foreach($tables as $table) 
		{
			if ($is_oracle)
				$qry = "
					SELECT 
						col.column_name,
						col.data_type,
						col.data_length,
						col.nullable,
						com.comments
					FROM 
						all_tab_columns col
					LEFT JOIN 
						all_col_comments com 
						ON col.owner = com.owner 
						AND col.table_name = com.table_name 
						AND col.column_name = com.column_name
					WHERE 
						col.owner = '".$database_name."'
						AND col.table_name = '".$table['name']."'
				";
			else
				$qry = 'SHOW FULL COLUMNS FROM '.$table['name'];

			$colInfo = DB::select($qry);
			$info_list = [];

			foreach($colInfo as $col) 
			{
				$field = $is_oracle ? $col->column_name : $col->Field;
				if ($is_oracle)
					$comment = $is_oracle ? $col->comments : $col->comment;
				else
					$comment = trim($col->Comment)?:(@$colCommentSet[strtolower($field)]?:'');
				$type = $is_oracle ? $col->data_type : $col->Type;
				$null = $is_oracle ? $col->nullable : ucfirst($col->Null);
				$key = $is_oracle ? null : ($col->Key=='PRI'?'PK':'');


				$info_list[] = [
					$field,
					$type,
					$null,
					$key,
					$comment,
				];
			}
			$info[] = [
				'no'     => (empty($no)?$no = 1:++$no),
				'name'    => $table['name'],
				'comment' => $table['comment'],
				'info'    => $info_list,
			];
		}
		return $info;
	}
// ==================================================
// Export HTML : 
// ==================================================
	/**
	 * Function: preview_datadic()
	 * Description: render รายงานออกมาเป็น HTML (เพื่อดูเป็นตัวอย่าง)
	 */
    public function preview_datadic() 
	{
		set_time_limit(30);

        $data = $this->get_data();
		return view('export-html.render', $data);
    }
	// ==================================================
	// Export Word : 
	// ==================================================
		/**
		 * Function: create_word()
		 * Description: ทำการ render เป็น word.
		 */
		private function create_word() 
		{
			$data = $this->get_data();
	
			$filename = 'DataDictionary_'.env('DB_DATABASE').'.docx';
	
			$phpWord = new \PhpOffice\PhpWord\PhpWord();
			$section = $phpWord->addSection();
			
			// START CONTNET
			$header = array('size' => 16, 'bold' => true);
			
			$rows = 10;
			$cols = 5;
	
			// $section->addText('Basic table', $header);
			// Style
			$tableStyleName     = 'StyleTest1';
			
			$tableStyle         = array(
				'borderSize'    => 6, 
				'borderColor'   => 'F73605', 
				'afterSpacing'  => 2, 
				'Spacing'       => 0, 
				'cellMargin'    => 0, 
				// 'cellMargin' => 1, 'cellSpacing' => 2
			);
			$firstRowStyle = array(
				'cellMargin'  => 1, 
				'cellSpacing' => 1, 
				'bgColor'     => 'deeaf6'
			);
	
			$fontStyle = array('name' => 'TH SarabunPSK', 'size' => 14);
	
			$FirstRowCellStyle = array_merge($fontStyle, ['bold' => true]);
			$cellStyle = $fontStyle;
	
			$cellBorderStyle = [
				'borderSize' => 6,
				'afterSpacing' => 20,
			];
	
	
			$phpWord->addTableStyle($tableStyleName, $tableStyle, $firstRowStyle);
	
			// Table Database list:
			$table =$section->addTable($tableStyleName);
			$table->addRow(360, ['exactHeight' => true]);
				$table->addCell(642.85, $cellBorderStyle)->addText('ลำดับ', $FirstRowCellStyle);
				$table->addCell(3828.57, $cellBorderStyle)->addText('Table Name', $FirstRowCellStyle);
				$table->addCell(3928.57, $cellBorderStyle)->addText('Description', $FirstRowCellStyle);
				$table->addCell(871.42, $cellBorderStyle)->addText('Remark', $FirstRowCellStyle);
	
				foreach($data['tables'] as $key => $item) 
				{
					$table->addRow(360, ['exactHeight' => true]);
						$table->addCell(null, $cellBorderStyle)->addText($key + 1, $cellStyle);
						$table->addCell(null, $cellBorderStyle)->addText($item['name'], $cellStyle);
						$table->addCell(null, $cellBorderStyle)->addText($item['comment'], $cellStyle);
						$table->addCell(null, $cellBorderStyle)->addText(null, $cellStyle);
				}
	
				// $section->addTextBreak(1);
				$section->addPageBreak();
	
			foreach($data['info'] as $item) 
			{
				// dd($item);
				$section->addText('ตารางที่ '.$item['no'].': '.$item['name'], $cellStyle);
				$section->addText('รายละเอียด: ตารางข้อมูล'.$item['comment'], $cellStyle);
	
				$table =$section->addTable($tableStyleName);
				$table->addRow(360, ['exactHeight' => true]);
					$table->addCell(2142, $cellBorderStyle)->addText('Field Name', $FirstRowCellStyle);
					$table->addCell(2142, $cellBorderStyle)->addText('Field Type', $FirstRowCellStyle);
					$table->addCell(1142, $cellBorderStyle)->addText('Allow Null', $FirstRowCellStyle);
					$table->addCell(642, $cellBorderStyle)->addText('KEY', $FirstRowCellStyle);
					$table->addCell(3214, $cellBorderStyle)->addText('Description', $FirstRowCellStyle);
	
					foreach($item['info'] as $info) {
						$table->addRow(360, ['exactHeight' => false]);
							$table->addCell(null, $cellBorderStyle)->addText($info[0], $cellStyle);
							$table->addCell(null, $cellBorderStyle)->addText($info[1], $cellStyle);
							$table->addCell(null, $cellBorderStyle)->addText($info[2], $cellStyle);
							$table->addCell(null, $cellBorderStyle)->addText($info[3], $cellStyle);
							$table->addCell(null, $cellBorderStyle)->addText($info[4], $cellStyle);
					}
				$section->addTextBreak(2);
			}
			$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
			$objWriter->save($filename);
			return response()->download(public_path($filename));
		}
// ==================================================
// Other : 
// ==================================================
    public function update_table_info() 
	{
        $data = $this->get_data();
        foreach($data['info'] as $item) 
		{
            $table = $item['name'];
            $info = $item['info'];

            DB::statement("ALTER TABLE ".$table." COMMENT = '".$item['comment']."'");
            foreach($info as $info_item) 
                DB::statement("ALTER TABLE `".$table."` CHANGE `".$info_item[0]."` `".$info_item[0]."` ".$info_item[1]." COMMENT '".$info_item[4]."' ");
        }        
    }

}
